package com.bncc.view;

public class Menu {
    public void displayMenu() {
        System.out.println("1. Generate Todo List");
        System.out.println("2. Add Todo");
        System.out.println("3. Get All Todo List");
        System.out.println("4. Update Task by ID");
        System.out.println("5. Get Todo List by Category");
        System.out.println("6. Delete Task by ID");
        System.out.println("7. Exit");
    }
}