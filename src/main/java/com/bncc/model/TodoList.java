package com.bncc.model;

import java.util.ArrayList;
import java.util.Scanner;

public class TodoList {
    private ArrayList<Task>taskList;

    public TodoList() {
        this.taskList = new ArrayList<>();
    }

    public void generateTodoList() {
        addTodo(1, "Makan Roti Canai", "Urgent");
        addTodo(2, "Belajar Menunggang Keledai", "School");
        addTodo(3, "Liburan ke Disneyland", "Fun");
        addTodo(4, "Menanam Pisang", "Urgent");
        addTodo(5, "Belajar Seruling Bambu", "School");
        System.out.println("Todo List is generated");
    }

    public void addTodoByInput() {
        Scanner userInput = new Scanner(System.in);

        System.out.println("Input ID: ");
        int taskId = userInput.nextInt();
        userInput.nextLine();

        System.out.println("Input Task Name: ");
        String taskName = userInput.nextLine();

        System.out.println("Input Task Category: ");
        String taskCategory = userInput.nextLine();

        addTodo(taskId, taskName, taskCategory);
    }

    public void addTodo(int taskId, String taskName, String taskCategory) {
        taskList.add(new Task(taskId, taskName, taskCategory));
    }

    public void displayAllTodoList() {
        System.out.println(getAllTodoList());
    }

    public String getTodoByIndex(int index) {
        Task selectedTask = taskList.get(index);
        return selectedTask.getTask();
    }

    public String getAllTodoList() {
        StringBuilder todoList = new StringBuilder();
        for (int i = 0; i < taskList.size(); i++) {
            todoList.append(getTodoByIndex(i));
            if (i < taskList.size() - 1) {
                todoList.append("\n");
            }
        }
        return todoList.toString();
    }

    public void selectTaskToUpdate() {
        displayAllTodoList();

        Scanner userInput = new Scanner(System.in);
        int updateId = userInput.nextInt();

        markTaskDoneById(updateId);
    }

    void markTaskDoneById(int taskId) {
        for (Task selectedTask : taskList) {
            if (selectedTask.isTask(taskId)) {
                selectedTask.markTaskDone();
                return;
            }
        }
    }

    public void inputTaskCategoryToDisplay() {
        displayAllTodoList();

        Scanner userInput = new Scanner(System.in);
        String taskCategory = userInput.nextLine();

        displayTodoListByCategory(taskCategory);
    }

    private void displayTodoListByCategory(String taskCategory){
        System.out.println(getTodoListByCategory(taskCategory));
    }

    public String getTodoListByCategory(String category) {
        StringBuilder todoList = new StringBuilder();

        for (int i = 0; i < taskList.size(); i++) {
            Task selectedTask = taskList.get(i);

            if (selectedTask.isTask(category)) {
                todoList.append(getTodoByIndex(i));

                if (i < taskList.size() - 1) {
                    todoList.append("\n");
                }
            }
        }
        return todoList.toString();
    }

    public void selectTaskToRemove() {
        displayAllTodoList();

        Scanner userInput = new Scanner(System.in);
        int removeId = userInput.nextInt();

        removeTaskById(removeId);
    }

    public void removeTaskById(int taskId) {
        for (int i = 0; i < taskList.size(); i++) {
            Task selectedTask = taskList.get(i);
            if (selectedTask.isTask(taskId)) {
                taskList.remove(i);
                return;
            }
        }
    }
}
