package com.bncc.model;

class Task {
    private int id;
    private String name;
    private boolean status;
    private String category;
    private String timestamp;

    Task(int taskId, String taskName, String taskCategory) {
        this.id = taskId;
        this.name = taskName;
        this.status = false;
        this.category = taskCategory;
    }

    String getTask(){
        return this.id + ". " + this.name + " [" + this.getStatus() + "]";
    }

    String getStatus() {
        String taskStatus;
        if (this.status) {
            taskStatus = "DONE";
        }
        else {
            taskStatus = "NOT DONE";
        }
        return taskStatus;
    }

    boolean isTask(int taskId) {
        return this.id == taskId;
    }

    boolean isTask(String taskCategory) {
        return this.category.equals(taskCategory);
    }

    void markTaskDone() {
        this.status = true;
    }
}
